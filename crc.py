# 89E012A6C40A24F721

import crc16
import crcmod.predefined
from binascii import unhexlify


def check(crc):
    # crc = crc[0:-2]
    s = unhexlify(crc)

    crc16 = crcmod.predefined.Crc('xmodem')
    crc16.update(s)
    # print(crc16.hexdigest())

    if crc16.hexdigest() == "0000":
        return True
    else:
        return False


def genarate(data):
    s = unhexlify(data)

    crc16 = crcmod.predefined.Crc('xmodem')
    crc16.update(s)


    return crc16.hexdigest() + '0A'  # example crc16.hexdigest()+'0A'      0A = \n     0D = \r
