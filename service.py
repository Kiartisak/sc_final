import time
import subprocess
import engine
import server

import Adafruit_GPIO.SPI as SPI
import Adafruit_SSD1306

from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont

# Raspberry Pi pin configuration:
RST = 24
# Note the following are only used with SPI:
DC = 23
SPI_PORT = 0
SPI_DEVICE = 0
disp = Adafruit_SSD1306.SSD1306_128_32(rst=RST)

# Initialize library.
disp.begin()

# Clear display.
#disp.clear()
#disp.display()

width = disp.width
height = disp.height
image = Image.new('1', (width, height))

# Get drawing object to draw on image.
draw = ImageDraw.Draw(image)

# Load default font.
font = ImageFont.load_default()

print(
    """    ------------------------------------------------------------------
    ******************************************************************
                      -+-+-+ Gateway ASON service +-+-+-
            :::: Author  ---- Brainwave Grooup CO.,LTD   ::::
           :::: Programmer ---- Jirakit Paitoonnaramit---- ::::
    ******************************************************************
    ------------------------------------------------------------------\n\n\n"""
)

print("Connecting....")
disp.clear()
disp.display()

draw.text((3,6),"Please wait...", font=font, fill=255)
disp.image(image)
disp.display()
time.sleep(2)

#cmd = "sudo ifconfig wwan0 down"
#subprocess.call(cmd , shell = True)
#cmd = "cd ~"
#subprocess.call(cmd , shell = True)
#cmd = "cd ~/umtskeeper/"
#subprocess.call(cmd , shell = True)
#cmd = "ls"
#subprocess.check_output(cmd , shell = True)
#cmd = "sudo ./umtskeeper --sakisoperators \"OTHER='CUSTOM_TTY' CUSTOM_TTY='/dev/ttyUSB4' APN='CUSTOM_APN' CUSTOM_APN='internet' APN_USER='true' APN_PASS='true'\" --sakisswitches \"--sudo --console\" --devicename 'Quectel' --log --nat 'no'"
#subprocess.call(cmd , shell = True)
# data
# test_data_raw = '89E012A6C40A24'
# test_data = '89E012A6C40A24F721'
# test_mac = 'E012A6C40A24'

test_gateway_id = 'testGW'

millis_buffer = 0
pulling_interval = 15 * 1000


def getTimer():
    millis = int(round(time.time() * 1000))
    return millis


millis_buffer = getTimer()

engine.getDeviceList(test_gateway_id)

lab = 0

while True:
    try:
        lab = lab + 1
       # print(lab)
        if(getTimer() - millis_buffer >= pulling_interval):
            # reset timer
        
            engine.getDeviceList(test_gateway_id)

            millis_buffer = getTimer()
            lab = 0

            # getting device in list
            

        engine.check_incomming_message()
        time.sleep(0.5)
    except:
        pass

