import server
import crc
import time
import json

import Adafruit_GPIO.SPI as SPI
import Adafruit_SSD1306

from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont

import serial

from binascii import *

# Raspberry Pi pin configuration:
RST = 24
# Note the following are only used with SPI:
DC = 23
SPI_PORT = 0
SPI_DEVICE = 0
disp = Adafruit_SSD1306.SSD1306_128_32(rst=RST)

disp.begin()
disp.clear()
disp.display()

width = disp.width
height = disp.height
image = Image.new('1', (width, height))

# Get drawing object to draw on image.
draw = ImageDraw.Draw(image)

# Load default font.
font = ImageFont.load_default()
#font = ImageFont.true('./ssd1306/fonts/Volter_28Goldfish_29.ttf',20)
is_received = True
msg = ""

ser = serial.Serial('/dev/ttyUSB0', 115200, timeout=10)
# ser = serial.Serial('/dev/cu.SLAB_USBtoUART', 115200, timeout=3)

count = 1


def getTimer():
    millis = int(round(time.time() * 1000))
    return millis


def getDeviceList(name):
    dv = server.getList(name)
    print("Number of Pending Devices is : " + str(len(dv)))

    for i in range(len(dv)):
        global count
        count = 1
        print("sending Request Message to : " + dv[i])
        code = generate_crc("89", dv[i])
        send_data(dv[i], code)
        time.sleep(1)


def generate_crc(fn_code, data):
    msg = fn_code+data
    crc_value = crc.genarate(msg)

    return msg + crc_value


def decode_value(msg, typ):
    msg = msg[0:-2]
    device_id = msg[2:14]
    length = int(msg[14:16], 16)
    length = int(length/2)

    value = {}
    for i in range(length):
        ten_val = msg[16+(i*4): 18+(i*4)]
        dec_val = msg[18+(i*4): 20+(i*4)]

        real_val = int(ten_val, 16) + (int(dec_val, 16)/100)

        value["val" + str(i+1)] = real_val

    # JSON Encoded Start :::
    data_buffer = {}
    data_buffer["device_id"] = device_id
    data_buffer["type"] = typ
    data_buffer["length"] = length
    data_buffer["value"] = value

    json_data = json.dumps(data_buffer)
    server.sendData(json_data)
    # END JSON Encoded :::


def resend(device, code):
    global count
    disp.clear()
    disp.display()
    draw.rectangle((0,0,width,height), outline=0, fill=0)
    draw.text((3,6),"Please wait...", font=font, fill=255)
    disp.image(image)
    disp.display()
    count += 1
    time.sleep(1)
    if count > 5:
        print("No Response data from : " + device)
        time.sleep(1)
    else:
        send_data(device, code)


def send_data(device, code):
    msg = ''
    global count

    try:
        # send data
        print("Send data command : " + code + " (" + str(count) + "/5)")
        ser.write(unhexlify(code))
     

        print("Send data compleate")
        time.sleep(0.5)

        print("Read data")
        read = ser.readline()
        print("Read data compleate")
        
        print(read)
        msg = (hexlify(read).upper()).decode('utf-8')
        msg = msg[:len(msg)-2]
        print(msg)

        try:
                if(crc.check(msg)):
                    if(msg[0:2] == "98"):
                        decode_value(msg, "status")                       
                        disp.clear()
                        disp.display()
                        draw.rectangle((0,0,width,height), outline=0, fill=0)
                        draw.text((3,6),"Status : OK", font=font, fill=255)
                        disp.image(image)
                        disp.display()
                        
                    elif(msg[0:2] == "99"):                   
                        code99 = generate_crc("88", msg[2:14])
                        # device = (msg[2:14])
                        ser.write(unhexlify(code99))
                        decode_value(msg, "alert")                   
                        disp.clear()
                        disp.display()
                        draw.rectangle((0,0,width,height), outline=0, fill=0)
                        draw.text((3,6),"Status : Alert..!", font=font, fill=255)
                        disp.image(image)
                        disp.display()
                        resend(device, code)
                    
            
                    else:
                        resend(device, code)
                        print("Error Header Message on : " + device)
                else:
                    resend(device, code)
                    print("CRC_false")
                    
        except:
            resend(device, code)
            print('msgerror')
            pass

    except:
        resend(device, code)


def check_incomming_message():
    msg = ''
 
    # while ser.in_waiting > 0:
    #     ser.close()
    #     ser.open()

    #     read_byte = ser.readline()
    #     msg = (hexlify(read_byte).upper()).decode('utf-8')

        # tim = getTimer()
        # read until correct
        # while read_byte is not None:
        #     read_byte = ser.readline()
        #     msg += (hexlify(read_byte).upper()).decode('utf-8')

        #     if(getTimer() - tim >= 1000):
        #         break

    if ser.in_waiting > 0:    
        # ser.close()
        # ser.open()

        read_byte = ser.readline()
        msg = (hexlify(read_byte).upper()).decode('utf-8')
        msg = msg[:len(msg)-2]
        print(msg)
            # crc check
        try:
            if(crc.check(msg)):
                if(msg[0:2] == "99"):                   
                    code = generate_crc("88", msg[2:14])
                    # device = (msg[2:14])
                    ser.write(unhexlify(code))
                    decode_value(msg, "alert")                   
                    disp.clear()
                    disp.display()
                    draw.rectangle((0,0,width,height), outline=0, fill=0)
                    draw.text((3,6),"Status : Alert..!", font=font, fill=255)
                    disp.image(image)
                    disp.display()
        
                else:
                    print("Error Header Message")
            else:
                pass    
        except: 
              pass
    else:
        pass                 
    
