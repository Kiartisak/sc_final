import requests
import time


def getList(gateway_id):

    deviceList = []
    print("Getting Device List from Server....")
    response = requests.get('http://119.59.127.34:8989/api/SFAM_GATEWAY_NODE_LIST?gateway_id=gw01')

    
    json_object = json.loads(response.text)
    for item in json_object:
        deviceList.append(item["device_id"])
        print (item["device_id"])

    print("Getting Device Complete")

    return deviceList


def sendData(data):
    print("Send data")
    print(data)

    headers = {'Content-type': 'application/json'}

    try:
        resp = requests.post(
            "http://119.59.127.34:8989/api/SFAM_DEVICE_FEED", data=data, timeout=5, headers=headers)
        print(resp)
    except:
        print("No Internet Connection")
